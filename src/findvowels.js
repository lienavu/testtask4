const findVowels = (str) => {
  // Your implementation
  // Read README.md file, if you not understand what to do
  let regExp = /[aeiou]/gi;
  let vowels;
  if (typeof str === "string") {
    if (str === "") {
      return "String is empty";
    }
    vowels = str.match(regExp);
    if (vowels) {
      return vowels.length;
    } else {
      return "String is not contains vowels";
    }
  }
  return "Passed argument is not a string";
};
module.exports = findVowels;
