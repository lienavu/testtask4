#### Find number of vowels in string

String are given. Implement a function that find numbers of vowels in string. If given argument not an array - "Passed argument is not a string". If given string is empty - "String is empty". If given string is not contains vowels - "String is not contains vowels". 

Exp:
- findVowels('rweqqwrAAAA') // 5
- findVowels('TRDDDFTRKKLLLPNM') // 'String is not contains vowels'
- findVowels('') // 'String is empty'
- findVowels([]) // 'Passed argument is not a string'

For run:

1. Clone project to a local repository
2. Run npm install
3. Implement function
4. Run npm test
<hr>